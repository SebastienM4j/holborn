Holborn
=======

Configuration de mes environnements de développement sous linux.


Développement
-------------

### Environnement technique

Ce projet utilise [Ansible](https://www.ansible.com/) pour automatiser la configuration des environnements.

* [Ansible](https://www.ansible.com/)
* [Ansible Documentation](http://docs.ansible.com/)
* [Ansible Galaxy](https://galaxy.ansible.com/)

### Installation

* Cloner ce projet `git clone git@gitlab.com:SebastienM4j/holborn.git`
* Installer [Ansible](https://www.ansible.com/) en suivant [cette procédure](http://docs.ansible.com/ansible/intro_installation.html)
    * Pour Ubuntu, exécuter simplement le script `./getstarted.sh`


Utilisation
-----------

### Backup ancienne machine

Un [script d'export](migration/export.sh) des clés SSH, et des bases postgres est disponible. Il fait également faire le tour des repository Git histoire de vérifier que tout a été commité et poussé avant de migrer.

Lancer `./migration/export.sh` éventuellement avec les options suivantes pour filtrer les opérations :

* `-s` ou `--ssh` : copie des clés SSH
* `-g` ou `--git` : vérification interactive des repository Git
* `-p` ou `--postgres` : backup des bases Postgres

### Installation de Linux sur VirtualBox

* Installer [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
    * Installer également l'Extension Pack
* Télécharger un ISO d'une distribution Ubuntu
* Créer une nouvelle VM VirtualBox de type Linux Ubuntu 64 bits
    * Configurer les ressources
    * Configurer les répertoires de l'hôte à monter automatiquement
* Démarrer la VM en lui fournissant l'ISO afin d'installer Ubuntu
* Après l'installation, installer les suppléments invités
    * Cf [page 70, § 4.2.2.1](http://download.virtualbox.org/virtualbox/UserManual_fr_FR.pdf)
    * `sudo apt-get install dkms`
    * Aller dans le répertoire de montage du CD
    * `sudo sh ./VBoxLinuxAdditions.run`

### Clé SSH

#### Copier une clé existante

> Si la clé est sur un disque du host pour une nouvelle VM VirtualBox, paramétrer le  répertoire en tant que dossier partagé, puis pouvoir y accéder ajouter le groupe `vboxsf` à l'utilisateur avec `sudo usermod -a -G vboxsf sebastien`

* Copier le répertoire `.ssh` dans le home `cp -R /media/sf_something/.ssh ~/`
* Puis changer les droits `sudo chmod 600 ~/.ssh/*`

#### Créer une nouvelle clé

* Créer un clé SSH `ssh-keygen -C "sebastienm4j@email.com"`
* Copier la clé publique `~/.ssh/id_rsa.pub` dans [GitLab](https://gitlab.com/profile/keys)

### Installer holborn

* Installer Git `sudo apt-get install git`
* Créer un répertoire pour accueillir le projet `mkdir -p /home/sebastien/workspace ; cd /home/sebastien/workspace`
* Cloner ce projet `git clone git@gitlab.com:SebastienM4j/holborn.git ; cd holborn`
* Installer [Ansible](https://www.ansible.com/) en suivant [cette procédure](http://docs.ansible.com/ansible/intro_installation.html)
    * Pour Ubuntu, exécuter simplement le script `./getstarted.sh`

### Première installation

Pour une première installation, il est conseillé de jouer le playbook [`home.yml`](home.yml) ou [`apologic.yml`](apologic.yml) en fonction de l'utilisation de la machine.

* Utilisation personnelle : `ansible-playbook --ask-become-pass --ask-vault-pass -i local.ini --limit home home.yml`
* Utilisation professionnelle : `ansible-playbook --ask-become-pass --ask-vault-pass -i local.ini --limit apologic apologic.yml`

### Les playbooks

Le projet est constitué d'un certain nombre de playbooks quasiment indépendants les uns des autres. Ils peuvent donc être joués unitairement. Ce découpage permet d'avoir des playbooks plus rapide à exécuter, plus simple à maintenir et ainsi plus utilisés.

Deux playbooks sont dédiés à une installation complète d'une machine et ne font qu'importer les autres playbooks en fonction de l'utilisation prévue (voir ci-dessus § `Première installation`).

| Playbook                               | [`home.yml`](home.yml) | [`apologic.yml`](apologic.yml) | Description                                          |
| -------------------------------------- | ---------------------- | ------------------------------ | ---------------------------------------------------- |
| [`base.yml`](base.yml)                 | X                      | X                              | Installation et configuration de base                |
| [`dev.yml`](dev.yml)                   | X                      | X                              | Installation des outils de développement communs     |
| [`java.yml`](java.yml)                 | X                      | X                              | Installation des outils de développement Java        |
| [`bash.yml`](bash.yml)                 | X                      | X                              | Alias et fonctions bash                              |
| [`holborn.yml`](holborn.yml)           | X                      | X                              | Configuration pour le projet Holborn                 |
| [`dotnet.yml`](dotnet.yml)             |                        |                                | Installation de .Net                                 |
| [`learning.yml`](learning.yml)         |                        |                                | Divers documents, apprentissage, ...                 |
| [`mansionhouse.yml`](mansionhouse.yml) | X                      |                                | Projet [Mansion House](https://gitlab.com/SebastienM4j/mansionhouse)                                                                                                      |
| [`golang.yml`](golang.yml)             | X                      |                                | Installation de Go                                   |
| [`gradle.yml`](gradle.yml)             | X                      |                                | Installation de mes projets Gradle et du SDK Groovy  |
| [`ansible.yml`](ansible.yml)           | X                      |                                | Installation de mes projets Ansible                  |
| [`underground.yml`](underground.yml)   | X                      |                                | Installation de mes projets "Underground"            |
| [`sebastienm4j.yml`](sebastienm4j.yml) | X                      |                                | Installation de mes projets de présentation          |
| [`lswa.yml`](lswa.yml)                 |                        | X                              | Installation du projet Apologic Solution Web         |
| [`costarica.yml`](costarica.yml)       |                        | X                              | Installation du projet Apologic DSN                  |
| [`spoutnik.yml`](spoutnik.yml)         |                        | X                              | Installation du projet Apologic BT v2                |
| [`onehome.yml`](onehome.yml)           |                        | X                              | Installation du projet Apologic OneHome              |
| [`fabrique.yml`](fabrique.yml)         |                        | X                              | Installation des projets Apologic de la team Fabrique |

Ces playbooks unitaires peuvent donc être joués à tout moment après une `Première installation` afin d'installer ou mettre à jour des outils spécifiques.

Un alias permet de simplifier la commande :

* Utilisation personnelle : `play home playbook.yml`
* Utilisation professionnelle : `play apologic playbook.yml`
