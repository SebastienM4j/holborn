#/bin/bash

while [ "$1" != "" ]; do
	case $1 in
		-s | --ssh )
			shift
			ACTION='ssh'
			;;
		-g | --git )
			shift
			ACTION='git'
			;;
		-p | --postgres )
			shift
			ACTION='postgres'
			;;
		* )
			echo "Unknowed parameter '$1'"
			exit 1
	esac
	shift
done

[ -z $ACTION ] && ACTION='all'

BACKUP_DIR=$HOME/downloads/holborn-backup


backup_ssh() {
        echo
        echo -e "\e[32m   + -------------------- +"
        echo -e "\e[32m   |  Backup des clés SSH |"
        echo -e "\e[32m   + -------------------- +"
        echo -e "\e[39m"

	rm -Rf $BACKUP_DIR/.ssh
	mkdir -p $BACKUP_DIR
	cp -R $HOME/.ssh $BACKUP_DIR

	ls -alF $BACKUP_DIR/.ssh
}

check_git() {
	echo
	echo -e "\e[32m   + --------------------------- +"
	echo -e "\e[32m   |  Vérification des repos Git |"
	echo -e "\e[32m   + --------------------------- +"
	echo -e "\e[39m"

	for i in $(find $HOME -type d -name '.git')
	do
		cd "$i/.."
	
		STATUS_LINES=$(git st | wc -l)
		if [ "$STATUS_LINES" -gt 3 ]; then
			echo
			echo
		
			echo -e "\e[35m===\e[39m"
			pwd

			echo -e "\e[36m---\e[39m"
			git st

			echo  -e "\e[36m---\e[39m"
			echo "Ce repository Git a été modifié, que souhaites tu faire ?"
			echo
			echo "  cp      : commiter et pousser tous les changements dans la branche courante"
			echo "  b       : ouvrir un shell pour faire toutes les actions nécessaires"
			echo "  <other> : ignorer et passer au repository suivant"
			read REP
	
			case $REP in
				"cp")
					git aa
					git commit
					git push
					;;
				"b")
					echo "Un shell va s'ouvrir, tu peut faire toutes les actions souhaitées"
					echo "Pour terminer et passer au repository suivant sorts avec 'exit'"
					bash
					;;
				*)
					echo "Ok, on ignore ce repository, pas de regrets ?..."
					;;
			esac
		fi
	done
}

backup_postgres() {
        echo
        echo -e "\e[32m   + --------------------------- +"
        echo -e "\e[32m   |  Backup des bases postgres  |"
        echo -e "\e[32m   + --------------------------- +"
        echo -e "\e[39m"

	rm -Rf $BACKUP_DIR/postgres
	mkdir -p $BACKUP_DIR/postgres
	
	for i in $(psql -U postgres -l -A | grep '|postgres|' | cut -d"|" -f 1 | grep -v postgres | grep -v template)
	do
		echo "Backup '$i' database"
		pg_dump -U postgres -O $i | gzip > $BACKUP_DIR/postgres/$i.sql.gz
	done

	echo
	ls -alF $BACKUP_DIR/postgres
}


case $ACTION in
	ssh)
		backup_ssh
		;;
	git)
		check_git
		;;
	postgres)
		backup_postgres
		;;
	all)
		backup_ssh
		check_git
		backup_postgres
		;;
	*)
		echo "Unknowed action '$ACTION'"
		exit 2
esac

