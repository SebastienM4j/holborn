#/bin/bash

while [ "$1" != "" ]; do
        case $1 in
                -p | --postgres )
                        shift
                        ACTION='postgres'
                        ;;
                * )
                        echo "Unknowed parameter '$1'"
                        exit 1
        esac
        shift
done

[ -z $ACTION ] && ACTION='all'

BACKUP_DIR=$HOME/downloads/holborn-backup


restore_postgres() {
        echo
        echo -e "\e[32m   + --------------------------------- +"
        echo -e "\e[32m   |  Restauration des bases postgres  |"
        echo -e "\e[32m   + --------------------------------- +"
        echo -e "\e[39m"

	for i in $(ls $BACKUP_DIR/postgres)
	do
		db=$(echo $i | sed -e 's|\(.*\)\.sql\.gz|\1|')

		echo "Restore '$db' database"

		EXISTS_DB=$(psql -U postgres -l -A | grep "$db|" | wc -l)
		if [ "$EXISTS_DB" -eq 0 ]; then
			createdb -U postgres $db
		fi
		zcat $BACKUP_DIR/postgres/$db.sql.gz | psql -U postgres -d $db

		vacuumdb -U postgres -z -d $db
	done
}

case $ACTION in
        postgres)
                restore_postgres
                ;;
        all)
                restore_postgres
                ;;
        *)
                echo "Unknowed action '$ACTION'"
                exit 2
esac
